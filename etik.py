"""Etik package"""


class InvalidFoolError(Exception):
    """Raised if fool is invalid."""
    pass


class InvalidManError(Exception):
    """Raised if man is invalid."""
    pass


def stupid_func(fool=None):
    """
    Return something.

    :param fool: Optional "fool" of the function
    :type fool: int or None
    :raise etik.InvalidFoolError: If the fool is invalid.
    :return: stupid things
    :rtype: list[str]
    """
    if not (isinstance(fool, int) or fool is None):
        raise InvalidFoolError("uh.. ", fool)

    out = 2 if fool is None else int(fool)
    return [str(i) for i in range(out)]


def stupid_func_2(man=None):
    """
    Return something else.

    :param man: Optional "man" of the function and Bert is the Best
    :type man: int or None
    :raise etik.InvalidManError: If the man is invalid.
    :return: stupid things
    :rtype: list[str]
    """
    if not (isinstance(man, int) or man is None):
        raise InvalidManError("uh.. ", man)

    out = 2 if man is None else int(man)
    return [str(2 * i) for i in range(out)]
