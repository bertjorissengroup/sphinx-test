.. raw :: html

      <style>
     .row-container {
        text-align: center;
        padding: 10px;
        display: block;
     }
     .row-element {
        display: inline-block;
        float: none;
        width: 210px;
        padding: 10px;
        vertical-align: top;
     }
     .parallax {
        background-image: url("https://quantum-kite.com/wp-content/uploads/2018/02/textura2.jpg");
        height: 600px;
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 10px;
        display: block;
        position: relative;
     }
     .parallax-center {
        position: absolute;
        left: 0;
        top: 30%;
        text-align: center;
        width: 100%;
     }
     .parallax-text {
        font-size: 30px;
        color: white;
     }
  </style>
  <div class="parallax">
     <div class="parallax-center">
        <b>
           <p class="parallax-text">HIGH-PERFORMANCE QUANTUM TRANSPORT SOFTWARE</h1>
        </b>
     </div>
  </div>
  <div class="row-container">
     <div class="row-element">
        <img src="https://quantum-kite.com/wp-content/uploads/2018/02/1486485585-aim-archery-focus-success-goal-objective-target_81181-e1518705035364.png" style="width:50%; height:50%;">
        <h3>ACCURACY</h3>
        <p>Green’s functions of complex molecules and materials are evaluated by means of an accurate real-space polynomial (spectral) expansion, which provides fine control over accuracy and energy resolution for all in-built target functions in both pristine and disordered systems.</p>
     </div>
     <div class="row-element">
        <img src="https://quantum-kite.com/wp-content/uploads/2018/02/scale-e1518652823296.png" style="width:50%; height:50%;">
        <h3>SCALABILITY & SPEED</h3>
        <p>KITE boasts a fast and highly-scalable state-of-the-art spectral algorithm, capable to treat extremely large computational domains with billions of atomic orbitals at a relatively modest computational cost. Target functions are evaluated by means of multi-threaded algorithms that can be easily optimised to specific CPU architectures.</p>
     </div>
     <div class="row-element">
        <img src="https://quantum-kite.com/wp-content/uploads/2018/02/aged_care_icon02.1512757902-e1518653613951.png" style="width:50%; height:50%;">
        <h3>VERSATILITY</h3>
        <p>KITE calculates a wide range of electronic properties (e.g., local DOS and optical conductivity) of empirical tight-tight binding (ETB) models with arbitrary complexity. A friendly python interface allows to easily define/import model parameters and select target functions/algorithms.</p>
     </div>
  </div>
  <div class="parallax">
     <div class="parallax-center">
        <p class="parallax-text">"KITE is powered by the same Green's function-based polynomial approach that has enabled us to treat record-large tight-binding models with 3.6 billion atoms in recently published work. The capability to deal with giant computational domains is essential in many research problems, from studies of quantum criticality in disordered lattices to simulations of electron transport in real-size devices made from two-dimensional crystals. KITE's project is a dream come true: a user-friendly open-source software combining state-of-the-art tight-binding methods and advanced spectral algorithms that hold the promise of opening new venues in computational modelling." (Aires Ferreira, Royal Society URF & Associate Professor of Physics, University of York, UK)</p>
     </div>
  </div>

.. toctree::

    Examples
    api


Indices and tables
==================

* :ref:`search`
