Bert is Better
==================

.. note:: This is just to show what is possible.

Installation
------------

To use Kite, first install it using:

.. code-block:: console

   (.venv) $ pip install quantum-kite-MAndelkovic

Example 1
---------

Here is a small example, though the output figure isn't right yet.

.. code-block:: python

    import kite
    import pybinding as pb
    import numpy as np
    import matplotlib.pyplot as plt

    pb.pltutils.use_style()

    lat.add_hoppings(
        # make an hopping between lattice site with a tuple
        # (relative unit cell index, site from, site to, hopping energy)
        ([1, 0], 'A', 'A', - 1 ),
        ([0, 1], 'A', 'A', - 1 )
    )

    configuration = kite.Configuration(divisions=[2, 2],
                                       length=[256, 256],
                                       boundaries=[True, True],
                                       is_complex=False,
                                       precision=1)

    calculation = Calculation(configuration)

    calculation.dos(num_points=1000,
                    num_random=10,
                    num_disorder=1,
                    num_moments=512)

    kite.export_lattice(lattice, configuration, calculation, filename='test.h5')

    kite.KITEx("test.h5")
    kite.KITEtools("test.h5")

.. image:: https://i0.wp.com/user-images.githubusercontent.com/39924384/41257230-fb584956-6dc3-11e8-9b4d-530b0cf3be6c.png?w=640&ssl=1

Creating recipes
----------------

Here is a kind of a description of a function, :py:func:`etik.stupid_func` here:

.. py:function:: stupid_func(fool=None)

    Return something

    :param fool: Optional "fool" of the function.
    :type fool: int or None
    :raise etik.InvalidFoolError: If the fool is invalid.
    :return: stupid things
    :rtype: list[str]

The exception

.. py:exception:: etik.InvalidFoolError

   Raised if the fool is invalid.

will be raised if it is a really stupid fool.

More tests on Etik
------------------

>>> import etik
>>> etik.stupid_func()
['0', '2']

This is AUTO
------------

This is auto-generated for the :py:func:`etik.stupid_func_2` here:

.. autofunction:: etik.stupid_func_2


and also this:

.. autoexception:: etik.InvalidManError